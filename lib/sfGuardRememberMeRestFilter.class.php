<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Processes the "remember me" cookie.
 * 
 * This filter should be added to the application filters.yml file **above**
 * the security filter:
 * 
 *    remember_me:
 *      class: sfGuardRememberMeFilter
 * 
 *    security: ~
 * 
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id$
 */
class sfGuardRememberMeRestFilter extends sfFilter
{
  /**
   * Executes the filter chain.
   *
   * @param sfFilterChain $filterChain
   */
  public function execute($filterChain)
  {
    $name = sfConfig::get('app_sf_guard_plugin_remember_header_name', 'x-auth-token');

    if (
      $this->isFirstCall()
      &&
      $this->context->getUser()->isAnonymous()
      &&
      $token = sfContext::getInstance()->getRequest()->getHttpHeader($name)
    )
    {
      $q = Doctrine_Core::getTable('sfGuardRememberKey')->createQuery('r')
            ->innerJoin('r.User u')
            ->where('r.remember_key = ?', $token);

      if(sfGuardUserTable::getInstance()->hasRelation('Profile')) {
        $q->leftJoin('u.Profile');
      }

      if ($q->count())
      {
        $user = $q->fetchOne()->getUser();
        $this->getContext()->getUser()->signIn($user);
        $this->getContext()->getUser()->setGuardUser($user);
      }
    }

    // Code to execute AFTER the action execution, before the rendering
    // Delete User-Session after every API-Request
    session_unset();
    session_destroy();

    $filterChain->execute();
  }
}
